#include <iostream>

using namespace std;

#include <iomanip>

template <typename T>
class Stack
{
private:
    T* data;
    int size;
    T top;
public:
    Stack(int = 10);
    ~Stack(); 
    bool push(const T);
    bool pop(); 
    void printStack();
};

int main()
{
    Stack <int> myStack(5);

    int ct = 0;
    while (ct++ != 5)
    {
        cout << "Enter elements: ";
        int temp;
        cin >> temp;
        myStack.push(temp);
    }

    myStack.printStack();

    cout << "\nDelete elements:\n";

    myStack.pop(); 
    myStack.pop(); 
    myStack.printStack(); 

    return 0;
}

template <typename T>
Stack<T>::Stack(int s)
{
    size = s > 0 ? s : 10;
    data = new T[size]; 
    top = -1; 
}

template <typename T>
Stack<T>::~Stack()
{
    delete[] data; 
}

template <typename T>
bool Stack<T>::push(const T value)
{
    if (top == size - 1)
        return false;

    top++;
    data[top] = value;

    return true; 
}

template <typename T>
bool Stack<T>::pop()
{
    if (top == -1)
        return false; 

    data[top] = 0; 
    top--;

    return true; 
}

template <typename T>
void Stack<T>::printStack()
{
    for (int ix = size - 1; ix >= 0; ix--)
        cout << "|" << setw(4) << data[ix] << endl;
}